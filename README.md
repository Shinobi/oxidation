# Oxidation

A lightweight wiki server using Markdown syntax and good old CamelCase.

It gets its inspiration from the good old original wiki engine and from Notion (great piece of software but ... non free)

Oh, I forgot to mention that the server is written in Rust.

## To Do

- a *trait* for a block
    - a block is the base item in oxidation
    - a page is block composed of blocks
    - a block has a title
    - a block has a content
    - a block has a list of tags
    - a list of out links (or not ...)

- kind of block
    - chapter
        - header
        - paragraphs
    - table
    - media file
    - *TBD*
